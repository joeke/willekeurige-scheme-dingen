;; TODO: ADT van boom implementeren m.b.v. zelfgemaakte <VectorList>

(define-class <VectorTree> ()
  (contents #:init-value 'nil))

(define-method (getSubPositionIndices (position <Position>)) ;; Plekken in de array teruggeven waar de kinderen van een bepaalde node moeten staan
  (let* ((positionIndex (getVectorIndex position))
	 (childPositionOneIndex (+ (* positionIndex 2) 1))
	 (childPositionTwoIndex (+ childPositionOneIndex 1)))
    (vector childPositionOneIndex childPositionTwoIndex)))

(define-method (getSubPositionIndices vectorIndex) ;; Voor als de index al bekend is
  (let* ((childPositionOneIndex (+ (* vectorIndex 2) 1))
	 (childPositionTwoIndex (+ childPositionOneIndex 1)))
    (vector childPositionOneIndex childPositionTwoIndex)))

(define-method (add (vectorTree <VectorTree>) parentElementIndex elementOne elementTwo)
  (let ((subpositionIndices (getSubPositionIndices elementIndex)))
    (
