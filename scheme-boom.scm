;; Implementatie van een boom m.b.v. verwijzingen en lijsten

(use-modules (oop goops)
	     (ice-9 optargs))

(define-class <Tree> ()
  (rootNode #:init-value 'nil #:getter getRoot #:init-keyword #:rootNode))

;; Algemeen
(define-method (size (t <Tree>))
  (sumlist (size-helper (getRoot t))))

(define-method (size-helper (p <Position>))
  (if (> (numChildren p) 0)
      (append (map size-helper (getChildren p)) '(1))
      1))

(define-method (isEmpty (t <Tree>))
  (eq? (getRoot t) 'nil))

(define-method (positions (t <Tree>))
  (positions-helper (getRoot t)))

(define-method (positions-helper (p <Position>))
  (if (> (numChildren p) 0)
      (append (map positions-helper (getChildren p)) (list p))
      p))

;; Positie-klasse (node)
(define-class <Position> ()
  (element #:init-value 'nil #:getter getElement #:init-keyword #:element)
  (parent #:init-value 'nil #:getter getParent #:init-keyword #:parent)
  (children #:init-value '() #:getter getChildren #:init-keyword #:children))

(define-method (numChildren (p <Position>))
  (length (getChildren p)))

;; Query methods
(define-method (isInternal (p <Position>))
  (> (numChildren p) 0))

(define-method (isExternal (p <Position>))
  (= (numChildren p) 0))

(define-method (isRoot (p <Position>))
  (eq? (getParent p) 'nil))


;; Hulpfuncties
(define* (sumlist lst #:optional (counter 0))
  (if (null? lst)
      counter
      (sumlist (cdr lst) (+ counter (car lst)))))
