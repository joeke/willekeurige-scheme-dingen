;; Blijkbaar heeft Guile al een VectorList datatype → (ice-9 vlist)

(use-modules (oop goops))


(define-class <VectorList> ()
  (contents #:init-form (make-vector 10 'nil) #:getter getContents #:setter setContents) ;; Java doet 10 als begingrootte, dus ik ook.
  (fillPointer #:init-value 0 #:getter getFillPointer #:setter setFillPointer))


(define-method (addToEnd (vectorList <VectorList>) element) ;; Werkt — Volgens mij niet per se in de ADT.
  (when (= (getFillPointer vectorList) (blalength vectorList))
    (increaseVectorListSize vectorList))
  (vector-set! (getContents vectorList) (getFillPointer vectorList) element)
  (incrementFillPointer vectorList))

;; (define-method (add (vectorList <VectorList>) index element) ;; TODO O(n) hier fixen; rekening houden met nil
;;   (if (= index (vector-length (getContents vectorList)))
;;       (addToEnd vectorList element)
;;       (let ((tempElementStorage (get vectorList index)))
;; 	(set vectorList index element)
;; 	(add vectorList (+ 1 index) tempElementStorage))))

(define-method (add (vectorList <VectorList>) index element) ;; Werkt zoals ik het voor ogen had. Geen idee of het ook volgens de List ADT is.
  (when (= index (blalength vectorList))
    (increaseVectorListSize vectorList))
  (cond ((eq? (get vectorList index) 'nil)
	 (set vectorList index element)
	 (when (> index (getFillPointer vectorList))
	   (setFillPointer vectorList (+ 1 index))))
	(else
	 (let ((elementToMove (get vectorList index)))
	   (set vectorList index element)
	   (add vectorList (+ 1 index) elementToMove)))))

(define-method (blalength (vectorList <VectorList>)) ;; Werkt, maar Guile zeurt als deze procedure gewoon "length" heet.
  (vector-length (getContents vectorList)))

(define-method (get (vectorList <VectorList>) index) ;; Werkt
  (vector-ref (getContents vectorList) index))

(define-method (set (vectorList <VectorList>) index element) ;; Werkt
  (vector-set! (getContents vectorList) index element))

(define-method (remove (vectorList <VectorList>) index) ;; Werkt
  (set vectorList index 'nil))

;; Hulpmethodes -- niet per se deel van ADT
(define-method (increaseVectorListSize (vectorList <VectorList>)) ;; Werkt
  (let* ((originalVector (getContents vectorList))
	 (newVector (make-vector (* 2 (vector-length originalVector)) 'nil)))
    (vector-copy! newVector 0 originalVector)
    (setContents vectorList newVector)))

(define-method (incrementFillPointer (vectorList <VectorList>)) ;; Werkt
  (setFillPointer vectorList (+ 1 (getFillPointer vectorList))))
